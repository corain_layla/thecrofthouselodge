<?php

    require 'prepend.php';

    $dirSlide = './img/slide/';
    $slide = scandir($dirSlide);

    /*cropping functionnality
    $newSlide = array();
    foreach ($slide as $key => $img) {

    	if ($key != 0 && $key != 1 && $img != "cropped") {

    		$im = imagecreatefromjpeg($dirSlide.$img);

			list($width, $height, $type, $attr) = getimagesize($dirSlide.$img);

			if($width > $height*(16/9)) {
				//cut width
				$x= ($width - ($height * (16/9)))/2;
				$im2 = imagecrop($im, ['x' => $x, 'y' => 0, 'width' => ($height*(16/9))-$x, 'height' => $height]);
			} else if ($width < $height*(16/9)) {
				//cut height
				$y = ($height -($width * (9/16)))/2;
				$im2 = imagecrop($im, ['x' => 0, 'y' => $y, 'width' => $width, 'height' => ($width*(9/16))-$y]);
			} else {
				$im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
			}

			if ($im2 !== FALSE) {
				imagejpeg($im2, $dirSlide."cropped/".$key.".jpeg");
				array_push($newSlide, $key.".jpeg");
			    imagedestroy($im2);
			}
			imagedestroy($im);
    	}
    }*/


    $smarty->assign('slides', $slide);

    $dirList    = './img/list/';
    $images = scandir($dirList);
    $smarty->assign('images', $images);

    $smarty->display('index.tpl');

?>
