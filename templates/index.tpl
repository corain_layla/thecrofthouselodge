{extends file="base.tpl"}

{block name="content"}

	<div class="row">
		<h2>Bienvenue au Crofthouse Lodge !</h2>
	    <div class="col-lg-7 mb-2">
		    <br>

		    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
			  <div class="carousel-indicators">

			  	{foreach from=$slides item=item key=key}
		          {if $item != ".."}
		            {if $item != "."}
		            	{if $key == 2}
			               <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide"></button>
						{else}
							<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{$key - 2}" class="active" aria-current="true" aria-label="Slide"></button>
						{/if}
		            {/if}
		          {/if}
		        {/foreach}

			  </div>
			  <div class="carousel-inner">

			    {foreach from=$slides item=item key=key}
		          {if $item != ".."}
		            {if $item != "."}
		            	{if $key == 2}
			               <div class="carousel-item active">
						      <img src="./img/slide/{$item}" class="d-block w-100" alt="...">
						    </div>
						{else}
							<div class="carousel-item">
						      <img src="./img/slide/{$item}" class="d-block w-100" alt="...">
						    </div>
						{/if}
		            {/if}
		          {/if}
		        {/foreach}

			  </div>
			  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="visually-hidden">Previous</span>
			  </button>
			  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="visually-hidden">Next</span>
			  </button>
			</div>
	    </div>

	    <div class="col-md mt-5 pt-3">
	    			<p>Dans un parc privé de 2000m² clos de murs, calme et paisible, The Crofthouse Lodge est un très ancien corps de ferme, luxueusement rénové et équipé pour votre confort. Cette dépendance du 17ème siècle, proche de toute commodités, est située sur le GR34, à 5 minutes à pied des plages, du club nautique ou de l'espace aquatique.</p>
				
				<div class="text-center mt-5">
					<a href="https://www.iroise-bretagne.bzh/hebergement/the-crofthouse-lodge/" target="_blank">
						<img src="./img/logo-iroise-bretagne.svg" class="img-fluid rounded" alt="...">
						<p class="text-muted text-center mt-2">Retrouvez-nous sur le site de l'Office de tourisme !</p>
					</a>
				</div>
	    </div>
	</div>
	<hr>
	<h4 style="color:#606A4A;">Le Lodge <small class="text-muted">Qu'est ce que c'est ?</small></h4>
	<hr>
	<p>Cette suite de 60m² très bien équipée est aménagée afin d'offrir un séjour agréable et reposant à deux personnes. La chambre dispose d'un lit double king size, séparable en 2 lits simples sur demande. La literie et les draps sont de grande qualité.</p>
	<p>Les meubles anciens ont été choisis avec soin pour vous offrir le confort nécessaire, et la décoration a été réfléchie afin que vous vous sentiez dans une atmosphère sereine et relaxante. Le lodge est situé dans l'enceinte d'une propriété privée et dispose de sa propre entrée. Tout est mis en oeuvre afin de préserver votre intimité.</p>
	<p>Le parking visiteurs se trouve lui aussi dans l'enceinte de la propriété.</p>

	<hr>
	<h4 style="color:#606A4A;">À faire <small class="text-muted">à Plougonvelin</small></h4>
	<hr>
	<p>À cinq minutes du lodge, accessibles à pied par de charmants petits chemins, se trouvent des activités intéressantes à proximité des commerces et de la plage du Trez'hir.</p>
	<div class="mt-4 row row-cols-1 row-cols-md-3">
		<div class="col text-center">
			<a href="https://bretagne.ffrandonnee.fr/html/4037/randonner-sur-le-gr-34" target="_blank">
				<img src="./img/a_faire/logo-gr34.jpeg" class="rounded" style="height: 200px; width: auto;" alt="...">
				<p class="text-muted text-center">GR34</p>
			</a>
		</div>
		<div class="col text-center">
			<a href="https://www.ucpa.com/centres-sportifs/piscine-plougonvelin" target="_blank">
				<img src="./img/a_faire/logo-ucpa.png" class="rounded" style="height: 200px; width: auto;" alt="...">
				<p class="text-muted text-center">Espace Aquatique</p>
			</a>
		</div>
		<div class="col text-center">
			<a href="https://www.nautisme.pays-iroise.bzh/nautisme-centres-nautiques/32592-centre-nautique-de-plougonvelin" target="_blank">
				<img src="./img/a_faire/logo-amp.png" class="rounded" style="height: 200px; width: auto;" alt="...">
				<p class="text-muted text-center">Base Nautique</p>
			</a>
		</div>
	</div>
{/block}