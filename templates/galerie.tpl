{extends file="base.tpl"}

{block name="content"}

	<h2 style="color:#606A4A;">Le Lodge <small class="text-muted">en images</small></h2>
	<hr>

	<div class="row row-cols-1 row-cols-lg-3">

    {foreach from=$images item=item key=key}
      {if $item != ".."}
        {if $item != "."}
          <div class="col">
            <img data-src="../img/list/{$item}" class="lazy img-fluid pt-4"  type="button" data-bs-toggle="modal" data-bs-target="#modal{$key}">
          </div>
        {/if}
      {/if}
    {/foreach}
  </div>

  <!-- Modals -->

  {foreach from=$images item=item key=key}
    {if $item != ".."}
      {if $item != "."}
        <div class="modal fade" id="modal{$key}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <img src="../img/list/{$item}" class="img-fluid rounded">
          </div>
        </div>
      {/if}
    {/if}
  {/foreach}
{/block}