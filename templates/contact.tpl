{extends file="base.tpl"}

{block name="content"}

	<h2 style="color:#606A4A;">Comment nous contacter</h2>
	<hr>

	<div class="row">
		<div class="col-xl-7">
			<p>Besoin d'une information? De faire une réservation? D'obtenir des directions? Vous pouvez nous contacter par mail ou téléphone aux coordonnées suivantes.</p>
			<br>

			<h5 style="color:#606A4A;">Coordonnées</h5>
			<hr>
			<p>Numéro de téléphone : +33 6 79 64 08 37</p>
			<p>Adresse mail : thecrofthouselodge@gmail.com</p>
			<br>
			
			<h5 style="color:#606A4A;">Adresse</h5>
			<hr>
			<p>The Crofthouse Lodge, 28 Impasse du Pradigou 29217, Plougonvelin</p>

			<div id="map" class="mb-5" style="width:100%;height:400px;"></div>
			
		</div>
		<div class="col-sm">
			<div class="airbnb-embed-frame" data-id="678865362993080805" data-view="home" data-hide-reviews="true">
				<a href="https://www.airbnb.fr/rooms/678865362993080805&amp;source=embed_widget">Voir sur Airbnb</a>
				<a href="https://www.airbnb.fr/rooms/678865362993080805&amp;source=embed_widget" rel="nofollow">Cottage de luxe sur la côte, à l&#x27;abri des vents</a>
				<script async="" src="https://www.airbnb.fr/embeddable/airbnb_jssdk"></script>
			</div>
		</div>
	</div>

{/block}