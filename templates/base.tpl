<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Crofthouse Lodge</title>
    <link rel="icon" type="image/png" href="./img/logo_white.png"/>

    <link href="../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css">

    <style>

    body {
      background-image: url("./img/background.jpeg");
      background-repeat: no-repeat;
      background-attachment: fixed;
    }

    @font-face {
      font-family: "Custom";
      src: url(../fonts/malgun.ttf) format("truetype");
    }

    p {
      text-align: justify;
      font-weight: bold;
      font-family: "Custom";
    }

    a {
      text-decoration: none;
      text-shadow: 1px 1px 1px white;
      font-family: "Custom";
    }

    h2, h4, h5 {
      color:#606A4A;
      text-shadow: 1px 1px 1px white;
      font-weight: bold;
      font-family: "Custom";
    }

    img:not([src]) {
      visibility: hidden;
    }

    .no-shadows {
      text-shadow:0px 0px 0px white;
      font-weight: normal;
    }

    </style>
    
  </head>

  <body onload="initialize()">

    <nav class="navbar navbar-expand-lg navbar-light sticky-top" style="background-color: #8A5746;">
      <div class="container-fluid">
        <a class="navbar-brand text-white no-shadows" href="./index.php">
          The Crofthouse Lodge
          <img src="./img/logo_white.png" alt="Logo" width="33" height="auto" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link text-white no-shadows" href="./index.php">Accueil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white no-shadows" href="./galerie.php">Galerie</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link text-white dropdown-toggle no-shadows" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Réserver
              </a>
              <ul class="dropdown-menu" style="background-color: #8A5746;">
                <li><a class="dropdown-item text-white no-shadows" href="https://www.airbnb.fr/rooms/678865362993080805" target="_blank">Airbnb</a></li>
                <li><a class="dropdown-item disabled no-shadows">Autres à venir</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white no-shadows" href="./contact.php">Contactez-nous</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="mt-4">
        {block name="content"}{/block}
      </div>
    </div>

    <nav class="navbar bg-dark mt-4">
      <div class="container-fluid d-flex justify-content-center">
        <a href="https://linktr.ee/laylacorain" target="_blank"><p class="text-white no-shadows">Layla CORAIN-GAILLARD - 2022</p></a>
      </div>
    </nav>

    
  </body>
</html>

<script src='https://unpkg.com/leaflet@1.3.3/dist/leaflet.js'></script>
<script src="../vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js" ></script>

<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.8.3/dist/lazyload.min.js"></script>
<script>

  (function () {
    var lazyLoad = new LazyLoad({
        elements_selector: ".lazy"
    });
  })();

</script>
    

<script type="text/javascript">
  {literal}

    function initialize() {
      var map = L.map('map').setView([48.355, -4.705], 12);

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);

      L.marker([48.355, -4.705]).addTo(map).bindPopup('The Crofthouse Lodge').openPopup();
    }
  {/literal}
</script>
